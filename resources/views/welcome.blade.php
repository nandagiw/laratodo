<!DOCTYPE html>
<html>
    <head>
        <title>Laravel To Do</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

        <style>
            form {
                display: inline-block;
            }
            .form-group {
                text-align: center;
                padding-bottom: 25px;
            }
            #todo {
                margin: 0 auto;
                width: 500px;
            }

            a.close {
            float: right;
            }
        </style>

    </head>
<body>
    <div class="form-group">
      
    <form role="form">
            <h1>LaraToDo - Nanda Prasetyo</h1>
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <input type="text" class="form-control" placeholder="Your Task" name="task" id="task">
    </form>
    <button type="button" class="btn btn btn-primary" id="add">Add</button>
    <button type="button" class="btn btn btn-danger" id="clear">Clear Text</button>
        </div>

        <div></div>
    <ul class="list-unstyled" id="todo"></ul>

    <script type="text/javascript">
    $(document).ready(function () {
        $("#add").on("click", function () {

        // $("body").on('click', '#todo a', function () {
        //     $(this).closest("ul").remove();
        // 
		
            var task = $("input[name=task]").val();
            // console.log(task);
            $.ajax({
                url: "{{ url('/pushtask') }}",
                method: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{'task':task,'_token':'{{csrf_token()}}'},
                type:'post',
                success:function(data) {

                    $('#_token').val(data.new_token);
                    $('#todo').append("<ul><li><input type='checkbox' name='push' id='push' value="+data.id+" />" + data.name + " <a href='#' class='close' aria-hidden='true'>&times;</a></li></ul>");

                }
            });

            $("body").on('click', '#todo a', function () {
        //     $(this).closest("ul").remove();
            });
	    });

        $("#clear").on("click", function () {
            $("input[name=task]").val('');
        });

        $("body").on('click', '#todo a', function () {
            $(this).closest("ul").remove();
        });
    });
    </script>
</body>
</html>
