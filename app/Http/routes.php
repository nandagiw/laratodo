<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// use DB;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/pushtask', function () {

    $temp_task = request()->get('task');
    $new_token = csrf_token();

    $arr_task = array(
        'name' => $temp_task,
        'status' => '1',
        'created_at' => date('Y-m-d H:i:s'),
    );

    \DB::table('todolist')->insert($arr_task);
    $id = \DB::getPdo()->lastInsertId();

    return response()->json(array(
        'id' => $id,
        'name' => $temp_task,
        'new_token' => $new_token,
    ));
});
