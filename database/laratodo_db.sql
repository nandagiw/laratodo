/*
SQLyog Ultimate v9.02 
MySQL - 5.7.19 : Database - laratodo_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`laratodo_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `laratodo_db`;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2018_12_08_092409_create_todolist_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `todolist` */

DROP TABLE IF EXISTS `todolist`;

CREATE TABLE `todolist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `todolist` */

insert  into `todolist`(`id`,`name`,`status`,`created_at`,`updated_at`) values (1,'abc','1',NULL,NULL),(2,'def','1','2018-12-09 15:37:12',NULL),(3,'ghi','1','2018-12-09 15:45:29',NULL),(4,'a','1','2018-12-09 15:46:05',NULL),(5,'a','1','2018-12-09 15:50:06',NULL),(6,'v','1','2018-12-09 15:50:11',NULL),(7,'vvv','1','2018-12-09 15:52:49',NULL),(8,'ddd','1','2018-12-09 15:52:58',NULL),(9,'qqq','1','2018-12-09 15:53:04',NULL),(10,'a','1','2018-12-09 15:55:29',NULL),(11,'mencuci baju','1','2018-12-09 15:55:41',NULL),(12,'belajar bahasa inggris','1','2018-12-09 15:55:48',NULL),(13,'bersih bersih rumah','1','2018-12-09 15:58:20',NULL),(14,'mencuci piring','1','2018-12-09 15:58:40',NULL),(15,'beli rumah','1','2018-12-09 15:58:48',NULL),(16,'mengaji','1','2018-12-09 15:59:20',NULL),(17,'membaca buku','1','2018-12-09 15:59:27',NULL),(18,'nanda','1','2018-12-09 16:02:39',NULL),(19,'bayu','1','2018-12-09 16:02:44',NULL),(20,'andry','1','2018-12-09 16:02:49',NULL),(21,'iim','1','2018-12-09 16:03:00',NULL),(22,'yosep','1','2018-12-09 16:03:11',NULL),(23,'putri','1','2018-12-09 16:03:39',NULL),(24,'nanda','1','2018-12-09 16:05:47',NULL),(25,'bayu','1','2018-12-09 16:05:54',NULL),(26,'aaaa','1','2018-12-09 16:06:24',NULL),(27,'bbbbb','1','2018-12-09 16:06:30',NULL),(28,'cccccc','1','2018-12-09 16:06:35',NULL),(29,'aaa','1','2018-12-09 23:34:54',NULL),(30,'nanda','1','2018-12-09 23:35:59',NULL),(31,'nanda prasetyo','1','2018-12-09 23:36:26',NULL),(32,'bayu nurendro','1','2018-12-09 23:36:49',NULL),(33,'nanda','1','2018-12-09 23:43:12',NULL),(34,'aaaa','1','2018-12-09 23:47:19',NULL),(35,'aaaaaa','1','2018-12-09 23:47:43',NULL),(36,'abc','1','2018-12-09 23:48:27',NULL),(37,'aaaa','1','2018-12-10 00:26:41',NULL),(38,'bbb','1','2018-12-10 00:27:04',NULL),(39,'ccc','1','2018-12-10 00:27:14',NULL),(40,'ddd','1','2018-12-10 00:27:26',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
